import sublime, sublime_plugin, socket, re




class SendToMayaCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect(('localhost', 2222))
		regions = self.view.sel()  

		for r in regions:
			txt = self.view.substr(r)
			txt = re.sub('//.*?(\r\n?|\n)|/\*.*?\*/', '', txt, re.S)
			txt = txt.replace('\n', ' ').replace('\r',' ')
			s.send(txt)

		s.close()

		# self.view.insert(edit, 0, "Hello, World!")
